'use strict';
const {getQueriesFrom} = require('@testing-library/nightwatch')

describe('second test suite', function() {

  before(async function() {
    await browser.maximizeWindow();
  });

  after(async function() {
    await browser.quit();
  });

  test('second test', async function(browser) {
    await browser.navigateTo('https://www.npmjs.com/');
    await browser.pause(5000);
    const {getByPlaceholderText} = getQueriesFrom(browser)
    const searchInput = await getByPlaceholderText('Search packages');
    console.log(searchInput);
    await browser.updateValue(searchInput, ["nightwatch", browser.Keys.ENTER]);
    await browser.pause(5000);
    await browser.ensure.urlContains('https://www.npmjs.com/search?q=nightwatch');
    await browser.ensure.titleIs("nightwatch - npm search");
    await browser.expect.element('#main h2').text.to.match(/(\d+) packages found/);
    await browser.pause(5000);
  });

});
