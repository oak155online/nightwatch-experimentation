'use strict';

module.exports = {
  url: 'https://www.bbc.co.uk',
  elements: {
    searchLink: 'a[href="/search"]'
  }
};
