'use strict';

const bbcSearchPage = require('./page-objects/bbcSearchPage');

describe('first test suite', function() {

  before(async function() {
    await browser.maximizeWindow();
  });

  after(async function() {
    await browser.quit();
  });

  test('first test', async function(bot) {
    const bbcHompage = bot.page.bbcHompage();

    await bbcHompage.navigate();
    await bot.pause(3000);
    await bbcHompage.click('@searchLink');
    await bot.pause(3000);
    await bot.expect.title().to.contain(bbcSearchPage.title);
    await bot.pause(3000);
  });

});
